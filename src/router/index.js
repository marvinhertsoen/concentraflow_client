import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: 'aggregaflow/',
  routes: [
    // {
    //   path: '/',
    //   name: 'Hello',
    //   // component: require('../pages/HomePage.vue').default
    //   component: Hello
    // },
    {
      path: '/',
      name: 'Accueil',
      component: require('../pages/HomePage.vue').default
    },
    {
      path: '/flux',
      name: 'Flux',
      component: require('../pages/FluxPage.vue').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
