import axios from 'axios'
import {BACKEND_SERVER} from '../../config'

export default {
  getCategories () {
    return axios
      .get(BACKEND_SERVER + '/api/category')
      .then(response => {
        return response.data
      })
      .catch(error => console.log(error))
  },
  getCategory (id) {
    return axios
      .get(BACKEND_SERVER + '/api/category/' + id)
      .then(response => {
        return response.data
      })
  }

}
