import axios from 'axios'
import {BACKEND_SERVER} from '../../config'

export default {
  // getFeeds () {
  //   return axios
  //     .get('http://127.0.0.1:8000/api/feed')
  //     .then(response => {
  //       return response.data.news
  //     })
  // },
  getFeed (id) {
    let address = BACKEND_SERVER + '/api/feed'
    address = (id) ? address + '/' + id : address
    return axios
      .get(address)
      .then(response => {
        return response.data.news
      })
  },
  getCategoryFeed (id) {
    let address = BACKEND_SERVER + '/api/feed/category/' + id
    console.log(address)
    return axios
      .get(address)
      .then(response => {
        return response.data.news
      })
      .catch(error => console.log(error))
  }
}
