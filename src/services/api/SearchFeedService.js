import axios from 'axios'
import {BACKEND_SERVER} from '../../config'

export default {
  findFeeds (term) {
    let address = BACKEND_SERVER + '/api/feedsearch/' + term
    return axios
      .get(address)
      .then(response => {
        return response.data
      })
  }
}
