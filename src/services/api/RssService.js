import axios from 'axios'
import {BACKEND_SERVER} from '../../config'

export default {
  AddRss (rss) {
    let address = BACKEND_SERVER + '/api/rss'
    return axios
      .post(address, JSON.stringify(rss))
      .then(response => {
        return response.data
      })
      .catch(error => console.log(error))
  },
  RemoveRss (id) {
    let address = BACKEND_SERVER + '/api/rss/' + id
    return axios
      .delete(address)
      .then(response => {
        return response.data
      })
      .catch(error => console.log(error))
  }
}
