// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons' // { faUserSecret } (import single icon)

Vue.component('font-awesome-icon', FontAwesomeIcon)
library.add(fas)

Vue.use(BootstrapVue)
Vue.config.productionTip = false
// Vue.http.headers.common['Access-Control-Allow-Origin'] = '*'

// window.bus = new Vue()
const bus = new Vue()
export default bus

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
